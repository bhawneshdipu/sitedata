<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;


class Data extends Model
{
    //
    protected $connection='mongodb';
    protected $collection='data';
    protected $casts = [
        'rank' => 'bigInteger'
    ];
}

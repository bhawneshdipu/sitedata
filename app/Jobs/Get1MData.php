<?php

namespace App\Jobs;

use App\Data;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use MongoDB\Client;

class Get1MData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 3600;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $date=new \DateTime();
        $date=$date->format('Y-m-d');


        ini_set('memory_limit','-1');
        set_time_limit(0);
        $url = "http://s3.amazonaws.com/alexa-static/top-1m.csv.zip";
        $zipFile = $date.".zip";
        $zipResource = fopen($zipFile, "w");
        // Get The Zip File From Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FILE, $zipResource);
        $page = curl_exec($ch);
        if(!$page) {
            echo "Error :- ".curl_error($ch);
        }
        chmod($zipFile, 0777);

        $zip = new \ZipArchive;
        $res = $zip->open($date.".zip");
        if ($res === TRUE) {
            $zip->extractTo(getcwd());
            $zip->close();
            echo 'woot!';
        } else {
            echo 'doh!';
        }
        chmod("top-1m.csv",0777);
        rename("top-1m.csv",$date.".csv");

        $file = fopen($date.".csv","r");
        $array=array();
        $date=new \DateTime();
        $day=$date->format("Y-m-d");

        Log::debug("Data insertion Started");
        $array=array();
        while(! feof($file))
        {
            $d=fgetcsv($file);
            $obj=new Data();
            $obj->rank=(int)$d[0];
            $obj->host=$d[1];
            $obj->date=now()->format('Y-m-d');
            $obj->save();


            $a=new \stdClass();
            $a->rank=$d[0];
            $a->host=$d[1];
            $array[]=$a;
        }

        Log::debug("Data insertion Ended");
        $mongoClient=new Client();
        $mongodata=$mongoClient->sitebackup->data;
        $mongodata->insertMany($array);


        fclose($file);

        curl_close($ch);




    }
}

<?php

namespace App\Http\Controllers;

use App\Jobs\Get1MData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MongoDB\Client;

class SiteDataController extends Controller
{
    //
    public function index(){

        print_r("Starting....");
        Get1MData::dispatch();
        print_r("Done....");

    }
}
